<?php

namespace App\Services\Implement;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserServiceImplement implements UserService
{
    public function register(string $name, string $email, string $password): bool
    {
        $user = new User();

        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make($password);

        if ($user->save()) {
            return true;
        }

        return false;
    }

    public function login(string $email, string $password): bool
    {
        $cred = Auth::attempt(["email" => $email, "password" => $password, 'deleted_at' => null]);
        if (!$cred) {
            return false;
        }
        return true;
    }

    public function logout(): bool
    {
        Auth::logout();
        return true;
    }

    public function getBalance(): float
    {
        return User::find(Auth::user()->id)->last_balance;
    }
}
