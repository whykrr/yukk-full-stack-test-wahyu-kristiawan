<?php

namespace App\Services\Implement;

use App\Models\Transaction;
use App\Models\TransactionCreditEvidence;
use App\Models\User;
use App\Services\TransactionService;
use DB;

class TransactionServiceImplement implements TransactionService
{
    public function doCredit($data): bool
    {
        $transaction = new Transaction();
        $transactionCreditEvidence = new TransactionCreditEvidence();

        $user = User::find(auth()->user()->id);

        $transaction->no = 'TRX-CR-' . date('YmdHis');
        $transaction->user_id = auth()->user()->id;
        $transaction->amount = $data["amount"];
        $transaction->type = 'credit';
        $transaction->description = $data["description"];

        $user->last_balance = $user->last_balance + $data["amount"];

        DB::beginTransaction();
        if (!$transaction->save()) {
            DB::rollBack();
            return false;
        }

        $transactionCreditEvidence->transaction_id = $transaction->id;
        $transactionCreditEvidence->image = $data['evidence'];

        if (!$transactionCreditEvidence->save()) {
            DB::rollBack();
            return false;
        }

        if (!$user->save()) {
            DB::rollBack();
            return false;
        }

        DB::commit();

        return true;
    }

    public function doDebit($data): array
    {
        $transaction = new Transaction();

        $user = User::find(auth()->user()->id);

        $transaction->no = 'TRX-DB-' . date('YmdHis');
        $transaction->user_id = auth()->user()->id;
        $transaction->amount = $data["amount"];
        $transaction->type = 'debit';
        $transaction->description = $data["description"];

        if ($data['amount'] > $user->last_balance) {
            return [
                'error' => 'insufficientBallance',
                'status' => false
            ];
        }

        $user->last_balance = $user->last_balance - $data["amount"];

        DB::beginTransaction();
        if (!$transaction->save()) {
            DB::rollBack();
            return [
                "status" => false,
            ];
        }

        if (!$user->update()) {
            DB::rollBack();
            return [
                'status' => false,
            ];
        }

        DB::commit();

        return [
            'status' => true,
        ];
    }
    function getDetail(string $id): Transaction|null
    {
        $transaction = Transaction::find($id);
        return $transaction;
    }
    function getHistory(int $page, int $perPage, array $filter)
    {
        $transactions = Transaction::where('user_id', auth()->user()->id);

        if (!empty($filter['type'])) {
            $transactions->where('type', $filter['type']);
        }

        if (!empty($filter['keyword'])) {
            $transactions->where(function ($query) use ($filter) {
                $query->where('no', 'like', "%$filter[keyword]%");
                $query->orWhereFullText('description', $filter['keyword']);
            });
        }

        $transactions = $transactions->orderBy('created_at', 'desc')
            ->paginate($perPage);

        return $transactions;
    }
}
