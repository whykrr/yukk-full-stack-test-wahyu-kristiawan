<?php

namespace App\Services;

use App\Models\Transaction;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface TransactionService
{
    /**
     * Create record credit
     *
     * @param array $data = [
     *  'amount' => double,
     *  'description' => string,
     *  'evidence' => string
     * ]
     * @return boolean
     */
    public function doCredit($data): bool;

    /**
     * Create record debit
     *
     * @param array $data = [
     *  'amount' => double,
     *  'description' => string
     * ]
     * @return array [
     *  'error' => string
     *  'status' => boolean
     * ]
     */
    public function doDebit($data): array;

    /**
     * get history transaction
     *
     * @param integer $page
     * @param integer $perPagep
     * @param array $filter
     * @return LengthAwarePaginator
     */
    public function getHistory(int $page, int $perPage, array $filter);

    /**
     * Undocumented function
     *
     * @param string $id
     * @return Transaction|null
     */
    public function getDetail(string $id): Transaction|null;
}
