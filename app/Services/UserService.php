<?php

namespace App\Services;

interface UserService
{
    public function register(string $name, string $email, string $password): bool;
    public function login(string $email, string $password): bool;
    public function logout(): bool;
    public function getBalance(): float;
}
