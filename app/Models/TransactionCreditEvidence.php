<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TransactionCreditEvidence
 *
 * @property string $transaction_id
 * @property string $image
 * @property-read \App\Models\Transaction|null $transactions
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCreditEvidence newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCreditEvidence newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCreditEvidence query()
 * @property string $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCreditEvidence whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCreditEvidence whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCreditEvidence whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCreditEvidence whereTransactionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TransactionCreditEvidence whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TransactionCreditEvidence extends Model
{
    use HasFactory, HasUuids;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transaction_credit_evidence';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_id',
        'image'
    ];

    public function transactions()
    {
        return $this->belongsTo(Transaction::class);
    }
}
