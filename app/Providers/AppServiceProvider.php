<?php

namespace App\Providers;

use DB;
use Laravel\Sanctum\Sanctum;
use App\Services\UserService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Services\TransactionService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Events\QueryExecuted;
use App\Services\Implement\UserServiceImplement;
use App\Services\Implement\TransactionServiceImplement;

class AppServiceProvider extends ServiceProvider
{
    public $singletons = [
        UserService::class => UserServiceImplement::class,
        TransactionService::class => TransactionServiceImplement::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Sanctum::ignoreMigrations();
        if (App::environment('local')) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::listen(function (QueryExecuted $query) {
            $binding = implode(',', $query->bindings);
            Log::info("Execute : $query->sql - [$binding]");
        });
    }
}
