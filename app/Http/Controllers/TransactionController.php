<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TransactionService;
use App\Services\UserService;
use Validator;

class TransactionController extends Controller
{
    private $svcTransaction, $svcUser;
    public function __construct(TransactionService $svc, UserService $svcUser)
    {
        $this->svcTransaction = $svc;
        $this->svcUser = $svcUser;
    }

    public function topupForm(Request $request)
    {

        return view("topupForm", ['request' => $request]);
    }
    public function transactionForm(Request $request)
    {
        return view("transactionForm", ['request' => $request]);
    }

    public function doTansaction(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'type' => "required",
            'amount' => "required|numeric|min:0",
            'description' => "required",
            'evidence' => "required_if:type,credit|image:jpg,jpeg,png"
        ], [
            "required_if" => "The evidence is Required",
            "image" => "The evidence only allowed .jpg .jpeg .png",
        ]);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        if ($request->input("type") == "credit") {
            // handle image
            $evidence = $request->file("evidence")->store('evidence', 'public');

            $data = [
                'amount' => $request->input('amount'),
                'description' => $request->input('description'),
                'evidence' => $evidence,
            ];

            $doCredit = $this->svcTransaction->doCredit($data);
            if ($doCredit) {
                return redirect()->to('/')->with('success', 'Topup Success')->withInput();
            }

            return redirect()->back()->with('error', 'Topup Fail');
        } else if ($request->input("type") == "debit") {
            $data = [
                'amount' => $request->input('amount'),
                'description' => $request->input('description'),
            ];

            $doCredit = $this->svcTransaction->doDebit($data);
            if ($doCredit['status']) {
                return redirect()->to('/')->with('success', 'Transaction Success')->withInput();
            }

            if ($doCredit['error'] == 'insufficientBallance') {
                return redirect()->back()->with('error', 'Insufficiant Ballance')->withInput();
            }

            return redirect()->back()->with('error', 'Transaction Fail')->withInput();
        }

        return redirect()->back()->with('error', 'Topup Fail')->withInput();
    }

    public function dashboard(Request $request)
    {
        $page = $request->input('page', 1);
        $perPage = $request->input('per_page', 10);

        $filter = [
            'type' => $request->input('type'),
            'keyword' => $request->input('k')
        ];
        $data['history'] = $this->svcTransaction->getHistory($page, $perPage, $filter);
        $data['balance'] = $this->svcUser->getBalance();
        $data['request'] = $request;

        return view('dashboard', $data);
    }

    public function detailTransaction(string $id)
    {
        $data['tr'] = $this->svcTransaction->getDetail($id);

        return view('detail', $data);
    }
}
