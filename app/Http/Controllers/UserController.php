<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    private $userService;
    public function __construct(UserService $svc)
    {
        $this->userService = $svc;
    }

    public function doLogin(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "email" => "required|email",
            "password" => "required",
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $email = $request->input('email');
        $password = $request->input('password');

        $login = $this->userService->login($email, $password);
        if ($login) {
            return redirect()->back()->with('success', 'Login Success');
        }

        return redirect()->back()->with('error', 'Login Failed');
    }

    public function doRegister(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate)->withInput();
        }

        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');

        $register = $this->userService->register($name, $email, $password);
        if ($register) {
            return redirect()->to('login')->with('success', 'Register Success');
        }

        return redirect()->back()->with('error', 'Register Failed');
    }

    public function doLogout(Request $request)
    {
        $this->userService->logout();
        return redirect()->to('/')->with('success', 'Logout Success');
    }
}
