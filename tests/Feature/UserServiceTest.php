<?php

namespace Tests\Feature;

use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserServiceTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_register()
    {
        $svc = $this->app->make(UserService::class);
        $register = $svc->register("Wahyu", "wahyu@email.com", "wahyu123");

        $this->assertIsBool($register);
        $this->assertTrue($register);
    }

    public function test_login()
    {
        $svc = $this->app->make(UserService::class);
        $login = $svc->login("wahyu@email.com", "wahyu123");

        $this->assertIsBool($login);
        $this->assertTrue($login);
    }
}
