<?php

namespace Tests\Feature;

use App\Models\User;
use Storage;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransactionControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_credit()
    {
        $user = User::find(1);
        $this->actingAs($user);

        Storage::fake("evidence");

        $data = [
            'type' => 'credit',
            'amount' => 25000,
            'description' => 'topup',
            'evidence' => UploadedFile::fake()->image('test_image.jpg'),
        ];

        $response = $this->post('/transaction', $data);
        $response->assertRedirect();
        $response->assertSessionHas('success', 'Topup Success');
        Storage::disk('public')->assertExists('evidence/' . $data['evidence']->hashName());
    }
}
