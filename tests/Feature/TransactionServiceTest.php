<?php

namespace Tests\Feature;

use App\Services\TransactionService;
use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransactionServiceTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_doCredit()
    {
        $svcUser = $this->app->make(UserService::class);
        $svc = $this->app->make(TransactionService::class);

        $svcUser->login('wahyu@email.com', 'wahyu123');

        $doCredit = $svc->doCredit([
            'amount' => 50000,
            'description' => 'test credit',
            'evidence' => '123.jpg',
        ]);

        $this->assertIsBool($doCredit);
        $this->assertTrue($doCredit);
    }

    public function test_doDebit()
    {
        $svcUser = $this->app->make(UserService::class);
        $svc = $this->app->make(TransactionService::class);

        $svcUser->login('wahyu@email.com', 'wahyu123');

        $doCredit = $svc->doDebit([
            'amount' => 20000,
            'description' => 'test debit',
        ]);

        $this->assertIsBool($doCredit['status']);
        $this->assertTrue($doCredit['status']);
    }

    public function test_doDebitFails()
    {
        $svcUser = $this->app->make(UserService::class);
        $svc = $this->app->make(TransactionService::class);

        $svcUser->login('wahyu@email.com', 'wahyu123');

        $doCredit = $svc->doDebit([
            'amount' => 60000,
            'description' => 'test debit',
        ]);

        $this->assertIsBool($doCredit['status']);
        $this->assertFalse($doCredit['status']);
        $this->assertEquals($doCredit['error'], 'insufficientBallance');
    }

    public function test_getDetail()
    {
        $svc = $this->app->make(TransactionService::class);

        $detailTransaction = $svc->getDetail('9b331c68-9134-420b-a717-d7848ed56a6f');

        $this->assertEquals($detailTransaction->id, '9b331c68-9134-420b-a717-d7848ed56a6f');
        $this->assertEquals($detailTransaction->evidence->image, '123.jpg');
    }

    public function test_getHistory()
    {
        $svc = $this->app->make(TransactionService::class);

        $detailTransaction = $svc->getHistory(1, 5);

        $this->assertEquals($detailTransaction->total(), 10);
        $this->assertEquals($detailTransaction->items()[0]->no, 'TRX-CR-1-20240128131208');
    }
}
