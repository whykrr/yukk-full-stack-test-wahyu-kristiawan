<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Yukk</title>

    <link rel="stylesheet" href="{{ url('asset/bootstrap.min.css') }}">
</head>

<body>
    <header class="mb-4">
        <nav class="navbar navbar-expand-lg bg-body-tertiary bg-light">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">Yukk</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('register') }}">Register</a>
                            </li>
                        @endguest
                        @auth
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('topup') }}">Top Up</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('transaction') }}">Transaction</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('logout') }}">Logout</a>
                            </li>
                        @endauth
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <main role="main">
        @yield('content')
    </main>

    <script src="{{ url('asset/bootstrap.bundle.min.js') }}"></script>
</body>

</html>
