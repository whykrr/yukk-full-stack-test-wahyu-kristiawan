@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Detail Transaction</h3>
                <div class="mb-2">
                    <b>{{ $tr->no }}</b>
                </div>
                <div class="mb-2">
                    @if ($tr->type == 'credit')
                        <div class="badge bg-success">Top Up</div>
                    @else
                        <div class="badge bg-danger">Transaction</div>
                    @endif
                </div>
                <div class="mb-2">
                    <b>Amount</b> : {{ 'Rp. ' . number_format($tr->amount, 0, ',', '.') }}
                </div>
                <div class="mb-2">
                    <b>Description</b> : {{ $tr->description }}
                </div>
                <div class="mb-2">
                    <b>Date</b> : {{ $tr->created_at->format('d/m/Y H:i:s') }}
                </div>
                @if ($tr->type == 'credit')
                    <div class="mb-2">
                        <b>Evidence</b> : <br><img src="{{ url($tr->evidence->image) }}" alt="Evidence">
                    </div>
                @endif
                <div class="mb-2">
                    <a href="{{ url('/') }}" class="btn btn-info">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
