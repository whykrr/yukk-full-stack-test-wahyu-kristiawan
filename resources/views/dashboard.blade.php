@extends('layout')

@section('content')
    <div class="container">
        <div class="row mb-4">
            <div class="col-6">
                <h4>Welcome </h4> {{ auth()->user()->name }}
            </div>
            <div class="col-6 text-end">
                <h4>Your Balance </h4> {{ 'Rp. ' . number_format($balance, 0, ',', '.') }}
            </div>
        </div>
        @if (Session::has('success'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-success" role="alert">
                        {{ $request->session()->get('success') }}
                    </div>
                </div>
            </div>
        @endif
        <div class="row mb-2">
            <div class="col-6">
                <h3>Transaction History</h3>
            </div>
            <div class="col-6">
                <form action="{{ url('/') }}" class="d-flex" role="search">
                    <select name="type" id="type" aria-label="search" class="form-control me-2">
                        <option value="">- Pilih -</option>
                        <option value="credit" @if ($request->input('type') == 'credit') selected @endif>Top Up</option>
                        <option value="debit" @if ($request->input('type') == 'debit') selected @endif>Transaction</option>
                    </select>
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" name="k"
                        value="{{ $request->input('k') }}">
                    <button class="btn btn-outline-success" type="submit">Filter</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-4">
                @if (count($history) != 0)
                    @foreach ($history as $h)
                        <div class="card mb-2">
                            <div class="card-body">
                                <div class="mb-2">
                                    <b>{{ $h->no }}</b>
                                </div>
                                <div class="mb-2">
                                    @if ($h->type == 'credit')
                                        <div class="badge bg-success">Top Up</div>
                                    @else
                                        <div class="badge bg-danger">Transaction</div>
                                    @endif
                                </div>
                                <div class="mb-2">
                                    <b>Amount</b> : {{ 'Rp. ' . number_format($h->amount, 0, ',', '.') }}
                                </div>
                                <div class="mb-2">
                                    <b>Description</b> : {{ $h->description }}
                                </div>
                                <div class="mb-2">
                                    {{ $h->created_at->format('d/m/Y H:i:s') }}
                                </div>
                                <div class="mb-2">
                                    <a href="{{ url('transaction/' . $h->id) }}" class="btn btn-info">Detail</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-12 mb-2">
                        <div class="btn-group">
                            @if ($history->hasPages())
                                @if ($history->currentPage() != 1)
                                    <a href="{{ $history->previousPageUrl() }}" class="btn btn-primary">Previous</a>
                                @endif
                                @if ($history->onFirstPage())
                                    <a href="{{ $history->nextPageUrl() }}" class="btn btn-primary">Next</a>
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="col-12 mb-2">
                        @php
                            $start = ($history->currentPage() - 1) * $history->perPage() + 1;
                            $end = $start - 1 + $history->count();
                        @endphp
                        {{ $start }} - {{ $end }} of {{ $history->total() }} records.
                    </div>
                @else
                    <div class="m-auto text-muted">
                        No history records
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
