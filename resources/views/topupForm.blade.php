@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-6 m-auto">
                <h3>Top Up</h3>
                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ $request->session()->get('error') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        @foreach ($errors->all() as $error)
                            - {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <form enctype="multipart/form-data" action="{{ url('transaction') }}" method="POST">
                    @csrf
                    <input type="hidden" name="type" value="credit">
                    <div class="mb-3">
                        <label for="amount" class="form-label">Amount</label>
                        <input type="number" name="amount" class="form-control" id="amount"
                            aria-labelledby="amountHelp" value="{{ old('amount') }}">
                        <div id="amountHelp" class="form-text">
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="description" class="form-label">Description</label>
                        <textarea name="description" class="form-control" id="description" aria-labelledby="descriptionHelp">{{ old('description') }}</textarea>
                        <div id="descriptionHelp" class="form-text">
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="evidence" class="form-label">Evidence</label>
                        <input type="file" name="evidence" class="form-control" id="evidence"
                            aria-labelledby="evidenceHelp">
                        <div id="evidenceHelp" class="form-text">
                        </div>
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary">Top Up</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
