@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-6 m-auto">
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        @foreach ($errors->all() as $error)
                            - {{ $error }}<br />
                        @endforeach
                    </div>
                @endif
                <form action="{{ url('login') }}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" name="email" class="form-control" id="email"
                            placeholder="name@example.com" aria-labelledby="emailHelp" value="{{ old('email') }}">
                        <div id="emailHelp" class="form-text">
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password"
                            aria-labelledby="passwordHelp" value="{{ old('password') }}">
                        <div id="passwordHelp" class="form-text">
                        </div>
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
