<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_credit_evidence', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('transaction_id', 36);
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('restrict');
            $table->text('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_credit_evidence');
    }
};
