<?php

use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('login');
})->middleware('guest')->name('login');
Route::get('/register', function () {
    return view('register');
})->middleware('guest')->name('register');
Route::post('register', [UserController::class, 'doRegister'])->middleware('guest');
Route::post('login', [UserController::class, 'doLogin'])->middleware('guest');
Route::get('logout', [UserController::class, 'doLogout'])->middleware('auth');

Route::get('/', [TransactionController::class, 'dashboard'])->middleware('auth');
Route::get('/topup', [TransactionController::class, 'topupForm'])->middleware('auth');
Route::get('/transaction', [TransactionController::class, 'transactionForm'])->middleware('auth');
Route::post('/transaction', [TransactionController::class, 'doTansaction'])->middleware('auth');
Route::get('/transaction/{id}', [TransactionController::class, 'detailTransaction'])->middleware('auth');
