# Yukk Full Stack Wahyu Kristiawan

## Authors

-   [@Gitlab](https://www.gitlab.com/whykrr) Wahyu Kristiawan

## Installation

-   Duplicate .env.example
-   Setting .env
-   Create database
-   Run migration

```bash
  php artisan migrate
```

-   Create the symbolic links for storage

```bash
  php artisan storage:link
```

-   done
